Role Create USER
=========

A brief description of the role goes here.

Requirements
------------

Change values vars to file 
``` bash
defaults/main.yaml

USER: 'YOUR_USER_NAME'
SSH_PUBLIC_KEY: 'YOUR_SSH_PUBLIC_KEY'
PASSWORD: 'HASH_YOUR_PASSWORD' # python3 -c "from passlib.hash import sha512_crypt; print(sha512_crypt.encrypt(input()))"
```