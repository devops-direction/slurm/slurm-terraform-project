prefix = "test"

vm_image_name = "nginx"
vm_image_version = "1"

vm_count = 3

labels = {
    "project" = "terraform-practice"
  }

resources = ({
    cores = 2
    memory = 4
})

cidr_blocks = [
    "10.112.10.0/24",
    "10.112.20.0/24",
    "10.112.30.0/24"
    ]
