TERRAFORM PROJECT
=========

A brief description of the role goes here.

Requirements
------------

Change values vars to file providers.tf
``` bash
token     = "YOUR_TOKEN"
cloud_id  = "YOUR_CLOUD_ID"
folder_id = "YOUR_FOLDER_ID"
```

Change values vars to file instance_group.tf
``` bash
folder_id           = "YOUR_FOLDER_ID"
service_account_id  = "YOUR_SERVICE_ACCOUNT_ID"
```